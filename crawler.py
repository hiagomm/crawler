import requests
from bs4 import BeautifulSoup
import json
from pymongo import MongoClient
baseUrl = "https://www.letras.mus.br"


def url(x): return baseUrl + x
def urlContent(x): return requests.get(url(x)).content


def getArtists():
    bruteHtml = BeautifulSoup(urlContent("/mais-acessadas/"), "html5lib")
    table = bruteHtml.find_all("ol", class_="top-list_art")
    conjunt = [a.find_all("a") for a in table]
    return [ [link.get("href") for link in subConjunt] for subConjunt in conjunt][0]


def getListOfMusics(content):
    bruteHtml = BeautifulSoup(content, "html5lib")
    table = bruteHtml.find_all("ul", class_="cnt-list")
    lines = [line.find_all("a") for line in table]
    links = [link.get("href") for link in lines[0]]
    return links


def trataTexto(texto):
    texto = str(texto)
    texto = texto.replace("<p>", "")
    texto = texto.replace("[", "")
    texto = texto.replace("]", "")
    texto = texto.replace("</p>, ", "\n\n")
    texto = texto.replace("</p>", "")
    texto = texto.replace("<br/>", "\n")
    return texto


def getLyricsOfMusicAndName(content):
    dic = {}
    bruteHtml = BeautifulSoup(content, "html5lib")
    letra = bruteHtml.find("div", class_="cnt-letra")
    letraText = [paragrafo for paragrafo in letra.find_all("p")]
    dic["lyrics"] = trataTexto(letraText)
    divName = bruteHtml.find("div", class_="cnt-head_title")
    name = divName.find("h1")
    dic["name"] = name.text
    return dic


def checkArtistIfExists(artists, pointer):
    cleanArtists = []
    for artist in artists:
        bdArtits = pointer.find({ artist[1:len(artist)-1] : { "$exists": True } })
        if len(list(bdArtits)) == 0:
            cleanArtists.append(artist)
    print(cleanArtists)
    return cleanArtists


def start(artists , artistPointer):
   
    listLinks = artists
    for link in listLinks:
        dic = {}
        musicList = getListOfMusics(urlContent(link))
        dic[link[1:len(link)-1]] = {}
        for music in musicList:
            musicDiv = getLyricsOfMusicAndName(urlContent(music))
            print(musicDiv["name"])
            dic[link[1:len(link)-1]][musicDiv["name"].replace(".","")] = musicDiv["lyrics"]
        artistPointer.insert_one(dic)
    return dic


client = MongoClient("mongodb://localhost:27017/")
db = client["Artists-And-Letters"]
artistPointer = db["artists"]


# checkArtistIfExists(getArtists(), artistPointer)
dic = start( checkArtistIfExists(getArtists(), artistPointer) , artistPointer)

